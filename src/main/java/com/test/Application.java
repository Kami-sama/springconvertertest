package com.test;

import com.test.converter.TestConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@Import(TestConfig.class)
public class Application{

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
