package com.test.dto;

public class TestModel {
    private Integer id;
    private String text1;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "id=" + id +
                ", text1='" + text1 + '\'' +
                '}';
    }
}
