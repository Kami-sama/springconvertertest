package com.test.dto;

public class TestDto {
    private String id;
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TestDto{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
