package com.test;

import com.test.converter.TestConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class TestConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private TestConverter converter;


    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(converter);
    }

}
