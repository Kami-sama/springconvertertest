package com.test.controller;

import com.test.converter.TestConverter;
import com.test.dto.TestDto;
import com.test.dto.TestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class TestController{

    @Autowired
    private TestConverter converter;

    @GetMapping
    public String test(TestModel dto){
        return dto.toString();
    }
}
