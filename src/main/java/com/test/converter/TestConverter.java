package com.test.converter;

import com.test.dto.TestDto;
import com.test.dto.TestModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TestConverter implements Converter<TestDto, TestModel> {

    @Override
    public TestModel convert(TestDto source) {
        TestModel model = new TestModel();
        model.setId(Integer.valueOf(source.getId()));
        model.setText1(source.getText() + " BLA");
        return model;
    }
}
